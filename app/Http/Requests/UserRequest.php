<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    public function authorize()
    {
        return true; // Set to true to allow all requests. You can implement authorization logic here if needed.
    }

    public function rules()
    {
        return match ($this->getMethod()) {
            'POST' => [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'password' => 'required|min:6',
            ],
            'PUT', 'PATCH' => [
                'name' => 'required',
                'email' => 'required|email|unique:users,email',
                'password' => 'nullable|min:6',
            ],
            default => [],
        };
    }
}
