<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $users = User::all();
        return response()->json($users);
    }


    /**
     * Store a newly created resource in storage.
     */
    public function store(UserRequest $request)
    {
        $user = User::create($request->validated());

        return response()->json($user, 201);
    }

    /**
     * Update the specified resource in storage.
     */

    public function update(UserRequest $request, User $user)
    {
        $user->update($request->validated());

        return response()->json($user);
    }

    /**
     * Display the specified resource.
     */

    public function destroy(User $user)
    {
        $user->delete();

        return response()->json(null, 204);
    }
}
